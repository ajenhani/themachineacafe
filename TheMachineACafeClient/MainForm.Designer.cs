﻿namespace TheMachineACafeClient
{
    partial class MainForm
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.labelNumBadge = new System.Windows.Forms.Label();
            this.textNumBadge = new System.Windows.Forms.TextBox();
            this.panelMachine = new System.Windows.Forms.Panel();
            this.checkBoxUserMug = new System.Windows.Forms.CheckBox();
            this.buttonPrepareCafe = new System.Windows.Forms.Button();
            this.labelUseMug = new System.Windows.Forms.Label();
            this.labelVolumeSucre = new System.Windows.Forms.Label();
            this.buttonDown = new System.Windows.Forms.Button();
            this.buttonUp = new System.Windows.Forms.Button();
            this.labelQteSucre = new System.Windows.Forms.Label();
            this.comboBoxTypeBoisson = new System.Windows.Forms.ComboBox();
            this.labelTypeBoisson = new System.Windows.Forms.Label();
            this.buttonValidate = new System.Windows.Forms.Button();
            this.buttonCancel = new System.Windows.Forms.Button();
            this.pictureBoxTea = new System.Windows.Forms.PictureBox();
            this.pictureBoxChoclate = new System.Windows.Forms.PictureBox();
            this.pictureBoxCafe = new System.Windows.Forms.PictureBox();
            this.panelMachine.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxTea)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxChoclate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxCafe)).BeginInit();
            this.SuspendLayout();
            // 
            // labelNumBadge
            // 
            this.labelNumBadge.AutoSize = true;
            this.labelNumBadge.Location = new System.Drawing.Point(149, 66);
            this.labelNumBadge.Name = "labelNumBadge";
            this.labelNumBadge.Size = new System.Drawing.Size(93, 13);
            this.labelNumBadge.TabIndex = 0;
            this.labelNumBadge.Text = "Numéro de Badge";
            // 
            // textNumBadge
            // 
            this.textNumBadge.Location = new System.Drawing.Point(259, 63);
            this.textNumBadge.Name = "textNumBadge";
            this.textNumBadge.Size = new System.Drawing.Size(134, 20);
            this.textNumBadge.TabIndex = 1;
            this.textNumBadge.TextChanged += new System.EventHandler(this.textNumBadge_TextChanged);
            // 
            // panelMachine
            // 
            this.panelMachine.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelMachine.Controls.Add(this.checkBoxUserMug);
            this.panelMachine.Controls.Add(this.buttonPrepareCafe);
            this.panelMachine.Controls.Add(this.labelUseMug);
            this.panelMachine.Controls.Add(this.labelVolumeSucre);
            this.panelMachine.Controls.Add(this.buttonDown);
            this.panelMachine.Controls.Add(this.buttonUp);
            this.panelMachine.Controls.Add(this.labelQteSucre);
            this.panelMachine.Controls.Add(this.comboBoxTypeBoisson);
            this.panelMachine.Controls.Add(this.labelTypeBoisson);
            this.panelMachine.Enabled = false;
            this.panelMachine.Location = new System.Drawing.Point(58, 164);
            this.panelMachine.Name = "panelMachine";
            this.panelMachine.Size = new System.Drawing.Size(402, 285);
            this.panelMachine.TabIndex = 2;
            this.panelMachine.Paint += new System.Windows.Forms.PaintEventHandler(this.panelMachine_Paint);
            // 
            // checkBoxUserMug
            // 
            this.checkBoxUserMug.AutoSize = true;
            this.checkBoxUserMug.Location = new System.Drawing.Point(231, 201);
            this.checkBoxUserMug.Name = "checkBoxUserMug";
            this.checkBoxUserMug.Size = new System.Drawing.Size(15, 14);
            this.checkBoxUserMug.TabIndex = 15;
            this.checkBoxUserMug.UseVisualStyleBackColor = true;
            // 
            // buttonPrepareCafe
            // 
            this.buttonPrepareCafe.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonPrepareCafe.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.buttonPrepareCafe.Location = new System.Drawing.Point(271, 236);
            this.buttonPrepareCafe.Name = "buttonPrepareCafe";
            this.buttonPrepareCafe.Size = new System.Drawing.Size(110, 35);
            this.buttonPrepareCafe.TabIndex = 14;
            this.buttonPrepareCafe.Text = "Valider";
            this.buttonPrepareCafe.UseVisualStyleBackColor = true;
            this.buttonPrepareCafe.Click += new System.EventHandler(this.buttonPrepareCafe_Click);
            // 
            // labelUseMug
            // 
            this.labelUseMug.AutoSize = true;
            this.labelUseMug.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelUseMug.Location = new System.Drawing.Point(7, 200);
            this.labelUseMug.Name = "labelUseMug";
            this.labelUseMug.Size = new System.Drawing.Size(212, 17);
            this.labelUseMug.TabIndex = 12;
            this.labelUseMug.Text = "Voulez-vous utilisez votre mug ?";
            // 
            // labelVolumeSucre
            // 
            this.labelVolumeSucre.AutoSize = true;
            this.labelVolumeSucre.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labelVolumeSucre.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelVolumeSucre.Location = new System.Drawing.Point(328, 129);
            this.labelVolumeSucre.Name = "labelVolumeSucre";
            this.labelVolumeSucre.Size = new System.Drawing.Size(18, 19);
            this.labelVolumeSucre.TabIndex = 10;
            this.labelVolumeSucre.Text = "0";
            // 
            // buttonDown
            // 
            this.buttonDown.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonDown.Location = new System.Drawing.Point(22, 119);
            this.buttonDown.Name = "buttonDown";
            this.buttonDown.Size = new System.Drawing.Size(60, 29);
            this.buttonDown.TabIndex = 9;
            this.buttonDown.Text = "-";
            this.buttonDown.UseVisualStyleBackColor = true;
            this.buttonDown.Click += new System.EventHandler(this.buttonDown_Click);
            // 
            // buttonUp
            // 
            this.buttonUp.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonUp.Location = new System.Drawing.Point(102, 119);
            this.buttonUp.Name = "buttonUp";
            this.buttonUp.Size = new System.Drawing.Size(60, 29);
            this.buttonUp.TabIndex = 8;
            this.buttonUp.Text = "+";
            this.buttonUp.UseVisualStyleBackColor = true;
            this.buttonUp.Click += new System.EventHandler(this.buttonUp_Click);
            // 
            // labelQteSucre
            // 
            this.labelQteSucre.AutoSize = true;
            this.labelQteSucre.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labelQteSucre.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelQteSucre.Location = new System.Drawing.Point(188, 129);
            this.labelQteSucre.Name = "labelQteSucre";
            this.labelQteSucre.Size = new System.Drawing.Size(123, 19);
            this.labelQteSucre.TabIndex = 7;
            this.labelQteSucre.Text = "Quantité de sucre";
            // 
            // comboBoxTypeBoisson
            // 
            this.comboBoxTypeBoisson.FormattingEnabled = true;
            this.comboBoxTypeBoisson.Location = new System.Drawing.Point(152, 46);
            this.comboBoxTypeBoisson.Name = "comboBoxTypeBoisson";
            this.comboBoxTypeBoisson.Size = new System.Drawing.Size(121, 21);
            this.comboBoxTypeBoisson.TabIndex = 6;
            // 
            // labelTypeBoisson
            // 
            this.labelTypeBoisson.AutoSize = true;
            this.labelTypeBoisson.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTypeBoisson.Location = new System.Drawing.Point(28, 49);
            this.labelTypeBoisson.Name = "labelTypeBoisson";
            this.labelTypeBoisson.Size = new System.Drawing.Size(113, 17);
            this.labelTypeBoisson.TabIndex = 5;
            this.labelTypeBoisson.Text = "Type de boisson";
            // 
            // buttonValidate
            // 
            this.buttonValidate.Location = new System.Drawing.Point(210, 110);
            this.buttonValidate.Name = "buttonValidate";
            this.buttonValidate.Size = new System.Drawing.Size(74, 36);
            this.buttonValidate.TabIndex = 3;
            this.buttonValidate.Text = "Valider";
            this.buttonValidate.UseVisualStyleBackColor = true;
            this.buttonValidate.Click += new System.EventHandler(this.buttonValidate_Click);
            // 
            // buttonCancel
            // 
            this.buttonCancel.Location = new System.Drawing.Point(366, 110);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(74, 36);
            this.buttonCancel.TabIndex = 4;
            this.buttonCancel.Text = "Annuler";
            this.buttonCancel.UseVisualStyleBackColor = true;
            this.buttonCancel.Click += new System.EventHandler(this.buttonCancel_Click);
            // 
            // pictureBoxTea
            // 
            this.pictureBoxTea.Image = global::TheMachineACafeClient.Properties.Resources.service_tea;
            this.pictureBoxTea.Location = new System.Drawing.Point(486, 215);
            this.pictureBoxTea.Name = "pictureBoxTea";
            this.pictureBoxTea.Size = new System.Drawing.Size(269, 235);
            this.pictureBoxTea.TabIndex = 7;
            this.pictureBoxTea.TabStop = false;
            this.pictureBoxTea.Visible = false;
            this.pictureBoxTea.Click += new System.EventHandler(this.pictureBoxTea_Click);
            // 
            // pictureBoxChoclate
            // 
            this.pictureBoxChoclate.Image = global::TheMachineACafeClient.Properties.Resources.service_choclate;
            this.pictureBoxChoclate.Location = new System.Drawing.Point(486, 217);
            this.pictureBoxChoclate.Name = "pictureBoxChoclate";
            this.pictureBoxChoclate.Size = new System.Drawing.Size(269, 235);
            this.pictureBoxChoclate.TabIndex = 16;
            this.pictureBoxChoclate.TabStop = false;
            this.pictureBoxChoclate.Visible = false;
            this.pictureBoxChoclate.Click += new System.EventHandler(this.pictureBoxChoclate_Click);
            // 
            // pictureBoxCafe
            // 
            this.pictureBoxCafe.Image = global::TheMachineACafeClient.Properties.Resources.service_cafe;
            this.pictureBoxCafe.Location = new System.Drawing.Point(486, 215);
            this.pictureBoxCafe.Name = "pictureBoxCafe";
            this.pictureBoxCafe.Size = new System.Drawing.Size(269, 235);
            this.pictureBoxCafe.TabIndex = 17;
            this.pictureBoxCafe.TabStop = false;
            this.pictureBoxCafe.Visible = false;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(783, 462);
            this.Controls.Add(this.pictureBoxChoclate);
            this.Controls.Add(this.pictureBoxTea);
            this.Controls.Add(this.pictureBoxCafe);
            this.Controls.Add(this.buttonCancel);
            this.Controls.Add(this.buttonValidate);
            this.Controls.Add(this.panelMachine);
            this.Controls.Add(this.textNumBadge);
            this.Controls.Add(this.labelNumBadge);
            this.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ForeColor = System.Drawing.SystemColors.ControlText;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "MainForm";
            this.Text = "Machine à café";
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.panelMachine.ResumeLayout(false);
            this.panelMachine.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxTea)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxChoclate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxCafe)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelNumBadge;
        private System.Windows.Forms.TextBox textNumBadge;
        private System.Windows.Forms.Panel panelMachine;
        private System.Windows.Forms.Button buttonValidate;
        private System.Windows.Forms.Button buttonCancel;
        private System.Windows.Forms.Label labelTypeBoisson;
        private System.Windows.Forms.ComboBox comboBoxTypeBoisson;
        private System.Windows.Forms.Label labelQteSucre;
        private System.Windows.Forms.Button buttonDown;
        private System.Windows.Forms.Button buttonUp;
        private System.Windows.Forms.Label labelVolumeSucre;
        private System.Windows.Forms.Label labelUseMug;
        private System.Windows.Forms.Button buttonPrepareCafe;
        private System.Windows.Forms.CheckBox checkBoxUserMug;
        private System.Windows.Forms.PictureBox pictureBoxTea;
        private System.Windows.Forms.PictureBox pictureBoxChoclate;
        private System.Windows.Forms.PictureBox pictureBoxCafe;
    }
}

