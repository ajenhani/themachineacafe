USE [MachineAcafe]
GO
/****** Object:  Table [dbo].[boisson]    Script Date: 25/09/2018 15:58:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[boisson](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[TypeBoisson] [varchar](30) NULL,
 CONSTRAINT [PK_boisson] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[userchoices]    Script Date: 25/09/2018 15:58:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[userchoices](
	[NumBadge] [varchar](10) NOT NULL,
	[TypeBoisson] [varchar](10) NULL,
	[QuantiteSucre] [int] NULL,
	[UtiliserSonMug] [int] NULL,
	[LastSelectionDate] [datetime] NULL,
 CONSTRAINT [PK_userchoices] PRIMARY KEY CLUSTERED 
(
	[NumBadge] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[boisson] ON 

INSERT [dbo].[boisson] ([Id], [TypeBoisson]) VALUES (1, N'Café ')
INSERT [dbo].[boisson] ([Id], [TypeBoisson]) VALUES (2, N'Thé')
INSERT [dbo].[boisson] ([Id], [TypeBoisson]) VALUES (3, N'Chocolat')
SET IDENTITY_INSERT [dbo].[boisson] OFF
INSERT [dbo].[userchoices] ([NumBadge], [TypeBoisson], [QuantiteSucre], [UtiliserSonMug], [LastSelectionDate]) VALUES (N'111123', N'Café ', 1, 1, CAST(N'2018-09-25T15:40:19.003' AS DateTime))
INSERT [dbo].[userchoices] ([NumBadge], [TypeBoisson], [QuantiteSucre], [UtiliserSonMug], [LastSelectionDate]) VALUES (N'123456', N'Chocolat', 1, 1, CAST(N'2018-09-25T15:40:41.710' AS DateTime))
